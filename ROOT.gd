extends Node2D
var power = 1000
var MAX_POWER = 1000
var unit_root
var unit01
var unit02
var unit03
var unit04
var unit05
var unit06
var buildCD = 5
var buildable = true
var c=0

var t = Timer.new()
var BOTNET = "null"
var headers=[
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883 Safari/537.36"
    ]

func _ready():
	set_fixed_process(true)
	get_tree().set_auto_accept_quit(false)
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	preloadAll()
	var s = "http://188.165.81.30/AllYourData.php?start="+str(OS.get_unix_time())
	#get_node("BOTNET").request(s,headers,false)
	pass
func preloadAll():
	unit_root = preload("res://sceny/UNIT.tscn")
	unit01 = preload("res://sceny/Unit01.tscn")
	unit02 = preload("res://sceny/Unit02.tscn")
	unit03 = preload("res://sceny/Unit03.tscn")
	unit04 = preload("res://sceny/Unit04.tscn")
	unit05 = preload("res://sceny/Unit05.tscn")
	unit06 = preload("res://sceny/Unit06.tscn")
	pass
func _fixed_process(delta):
	c+=delta
	losePower(delta)
	get_node("ForeGround/power").set_value(power)
	if(power<=0):
		lose()
	if(buildCD > 0):
		buildCD-=delta
	else:
		buildable=true
	pass
func losePower(number):
	power -= number
	pass
func addPower(number):
	power +=number
	power = min(power, MAX_POWER)
	pass
func lose():
	print("pszgrales koniec gry")
	self.get_parent().remove_child(self)
	pass
func build(unit,cost):
	if(buildable):
		var U = unit_root.instance()
		U.setPlug(unit)
		U.set_global_pos(Vector2(10,380)) #+randi()%41+1))
		get_node("Playzone").add_child(U)
		losePower(cost)
		buildable=false
		buildCD=1.5
	pass
	
func _on_BuyUnit1_pressed():
	var unit = unit01.instance()
	var cost = 10
	build(unit,cost)
	pass 

func _on_BuyUnit2_pressed():
	var unit = unit02.instance()
	var cost = 20
	build(unit,cost)
	pass # replace with function body

func _on_BuyUnit3_pressed():
	var unit = unit03.instance()
	var cost = 30
	build(unit,cost)
	pass # replace with function body

func _on_BuyUnit4_pressed():
	var unit = unit04.instance()
	var cost = 40
	build(unit,cost)
	pass # replace with function body

func _on_BuyUnit5_pressed():
	var unit = unit05.instance()
	var cost = 50
	build(unit,cost)
	pass # replace with function body

func _on_BuyUnit6_pressed():
	var unit = unit06.instance()
	var cost = 60
	build(unit,cost)
	pass # replace with function body


func _on_BOTNET_request_completed( result, response_code, headers, body ):
	print(result,", resp: ",response_code,", data: ",body,"->",body.get_string_from_utf8())
	if(BOTNET == "null"):
		if(result == 0):
			BOTNET = body.get_string_from_utf8()
			print(BOTNET)
			print("telemetry server notified")
	else:
		if(result == 0):
			BOTNET = body.get_string_from_utf8()
			print(BOTNET)
	pass # replace with function body

func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		var s = "http://188.165.81.30/AllYourData.php?s="+BOTNET+"&e="+str(OS.get_unix_time())
		#get_node("BOTNET").request(s,headers,false)
		t.start()
		yield(t, "timeout")
		print("telemetry server notified")
		get_tree().quit()

func open(numb):#obiekt buttona
	var what=get_node("Interface/BuyUnit"+str(numb))
	what.get_node("covers/AnimationPlayer").play("opening")
	what.set_disabled(false)
	print("button "+str(numb)+" activated")
	pass
