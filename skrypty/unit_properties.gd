extends Node

export var MAX_HEALTH = 100
export var AI_TYPE = 1
export var SIDE = 1
export var HEIGHT = 0
export var SPEED = 2.01
export var DAMAGE = 15
export var POWER_GEN = 0.002
