extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var root
var camera 
var targetZoom = Vector2(0.99,0.99)
var nowZoom
var zoomed=true
func _ready():
	root = preload("res://ROOT.tscn")
	camera = get_node("Camera2D")
	get_node("Root").set_pause_mode(PAUSE_MODE_STOP)
	print(get_node("Root").get_pause_mode())
	set_fixed_process(true)
	pass
func _fixed_process(delta):
	if(!zoomed):
		zoom()
	pass

func zoom():
	nowZoom = camera.get_zoom()
	if(nowZoom<=targetZoom):
		nowZoom.x+=0.01
		nowZoom.y+=0.01
		camera.set_zoom(nowZoom)
	else:
		zoomed=true
		startGame()
		pass

func startGame():
	self.remove_child(get_node("StartButton"))
	get_node("Root").open(1)
	get_node("Root").open(2)
	get_node("Root/Playzone/spawner").begin()
	pass

func _on_Button_pressed():
	zoomed=false
	print(camera.get_zoom())
	pass # replace with function body
	
